# Additional tweak

# This is to make HOME directory executive visible
# If you have already defined your own .bashrc file,
# just add this line to the last of your own .bashrc file.
export PATH=$PATH:/data/data/com.termux/files/home

# Author: Aravind Swami [github: name_is_cipher]
# Mail: aravindswami135@gmail.com

